<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Littlelunch\AddressValidator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class Validator extends AbstractHelper
{

    protected $PATH_TO_GOOGLE = 'https://maps.googleapis.com/maps/api/geocode/json?address=#ADDRESS#';

    protected $input_address;

    protected $google_address;

    public function setInputAddress($street, $city, $country, $postcode) {
        $this->input_address['street'] = $street;
        $this->input_address['city'] = $city;
        $this->input_address['country'] = $country;
        $this->input_address['postcode'] = $postcode;
    }

    public function getInputAdress() {
        return $this->input_address;
    }

    public function loadAddressByGoogle() {
        $address = $this->getInputAdressString();

        $requst = str_replace('#ADDRESS#', $address, $this->PATH_TO_GOOGLE);

        $result_json = file_get_contents($requst);

        $result = json_decode($result_json);
        if (!empty($result)) {
            $this->google_address = $this->transformGoogleAddress($result);
        }

    }

    public function compareAddress() {
        $flag = false;
        if (isset($this->google_address[0])) {
            $google_compare_address = $this->google_address[0];
            unset($google_compare_address['region']);
            if (json_encode($google_compare_address) === json_encode($this->input_address)) {
                $flag = true;
            }
        }
        return $flag;
    }

    public function getAddressByGoogle() {
        return $this->google_address;
    }

    protected function transformGoogleAddress($result) {
        $get_add_array = array();
        if (property_exists($result, 'results') and 
            !empty($result->results) ) {
            foreach ($result->results as $key => $value) {
                if(property_exists($value, 'address_components') and
                   !empty($value->address_components)) {
                    $get_add_tmp = array();
                    foreach ($value->address_components as $index => $data) {
                        if (in_array('street_number', $data->types)) {
                            $get_add_tmp['street_number'] = $data->long_name;
                        }
                        if (in_array('route', $data->types)) {
                            $get_add_tmp['street'] = $data->long_name;
                        }
                        if (in_array('locality', $data->types)) {
                            $get_add_tmp['city'] = $data->long_name;
                        }
                        if (in_array('postal_code', $data->types)) {
                            $get_add_tmp['postcode'] = $data->long_name;
                        }
                        if (in_array('country', $data->types)) {
                            $get_add_tmp['country'] = $data->long_name;
                        }
                        if (in_array('administrative_area_level_1', $data->types)) {
                            $get_add_tmp['region'] = $data->long_name;
                        }
                    }
                    $get_add_tmp['street'] = $get_add_tmp['street'].' '.$get_add_tmp['street_number'];
                    unset($get_add_tmp['street_number']);
                    array_push($get_add_array, $get_add_tmp);
                }
            }
        }
        return $get_add_array;
    }

    protected function getInputAdressString() {
        $string_address = str_replace(' ', '+', $this->input_address['street']).'+';
        $string_address .= $this->input_address['city'].'+';
        $string_address .= $this->input_address['postcode'].'+';
        if (!empty($region) and $region != '') {
            $string_address .= $this->input_address['region'].'+';
        }
        $string_address .= $this->input_address['country'];
        return $string_address;
    }

}
